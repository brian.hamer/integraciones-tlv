# Test FIF Integraciones TLV - Test 2

Se requiere una funcion en GO que lea una cadena de caracteres la cual contiene multiples campos en el formato TLV y genere un map[string]string con los campos TLV encontrados en la cadena.

# Función DecodeBuffer

Es la encarga de recibir un arreglo de Bytes y devolver un map[string]string y un error.

# Errores

-Si el arreglo es vacio
-Si no cumple con el tamaño mínimo requerido
-Si los primeros 2 characteres no son int
-Si el type no es N o A
-Si la información brindada no es correcta

# Interaccion - consola - run

1) go run main.go
2) ingresar el buffer por pantalla
3) recibir respuesta

# Testing

Comando: go test ./... -cover
