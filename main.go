package main

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strconv"
)

const (
	bufferMinTotal = 7
	sizeLarge      = 2
	offsetType     = 3
	minRequire     = 5
	alfa           = "Alfanumérico"
	num            = "Numérico"
	asciiZero      = 47
	asciiNine      = 58
	asciiA         = 64
	asciiZ         = 91
)

func main() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter Buffer: ")
	buffer, _ := reader.ReadString('\n')

	response, err := DecodeBuffer([]byte(buffer))
	if err != nil {
		fmt.Println(err)
		return
	}

	for key, value := range response {
		fmt.Printf("Campo: %s %s\n", key, value)
	}
}

func DecodeBuffer(bufferByte []byte) (map[string]string, error) {
	buffer := string(bufferByte)
	response := make(map[string]string)

	err := checkBufferSize(buffer)
	if err != nil {
		return nil, err
	}

	for {
		if len(buffer) <= 1 {
			break
		}

		lengthString, largeInt, err := getLarge(buffer)
		if err != nil {
			return nil, err
		}

		offsetValue := largeInt + sizeLarge
		value := buffer[sizeLarge:offsetValue]

		offsetTypeWord := offsetType + offsetValue
		typeWord := buffer[offsetValue:offsetTypeWord]
		typeData, err := getTypeData(value, typeWord[0:1])
		if err != nil {
			return nil, err
		}

		typeValue := typeWord[1:offsetType]

		stringFinal := fmt.Sprintf("%v%v%v%v%v%v", " de tipo ", typeData, " de largo ", lengthString, " y valor ", value)
		response[typeValue] = stringFinal
		buffer = buffer[offsetTypeWord:]
	}

	return response, nil
}

func checkBufferSize(buffer string) error {
	if len(buffer) <= 1 {
		return errors.New("Error, buffer could not be empty")
	}
	if len(buffer) < bufferMinTotal {
		return errors.New("Error, buffer size is not correct, missing data")
	}
	return nil
}

func getTypeData(value, typeData string) (string, error) {
	switch typeData {
	case "A":
		typeData = alfa
		for _, e := range value {
			if !((asciiZero < e && e < asciiNine) || (asciiA < e && e < asciiZ)) {
				return "", errors.New("Value does not match Alfanumeric type")
			}
		}
	case "N":
		typeData = num
		for _, e := range value {
			if !(asciiZero < e && e < asciiNine) {
				return "", errors.New("Value does not match Numeric type")
			}
		}
	default:
		return "", errors.New("Unknow type, must be Numeric or Alfanumeric, not: " + typeData)
	}
	return typeData, nil
}

func getLarge(buffer string) (string, int, error) {
	lengthString := buffer[0:sizeLarge]
	length, errL := strconv.Atoi(lengthString)
	if errL != nil {
		return "", 0, errors.New("Large cannot be parse string to int: " + lengthString)
	}

	if len(buffer)-minRequire < length {
		return "", 0, errors.New("Error, missing data")
	}
	return lengthString, length, nil
}
