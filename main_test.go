package main

import (
	"errors"
	"fmt"
	"reflect"
	"testing"
)

func TestDecodeBuffer(t *testing.T) {
	responseOne := make(map[string]string)
	responseOne["05"] = " de tipo Alfanumérico de largo 11 y valor AB398765UJ1"
	responseTwo := make(map[string]string)
	responseTwo["23"] = " de tipo Numérico de largo 02 y valor 00"
	responseThree := make(map[string]string)
	responseThree["05"] = " de tipo Alfanumérico de largo 11 y valor AB398765UJ1"
	responseThree["23"] = " de tipo Numérico de largo 02 y valor 00"

	var tests = []struct {
		buffer       []byte
		wantResponse map[string]string
		wantError    error
	}{
		{[]byte("11AB398765UJ1A05"), responseOne, nil},
		{[]byte("0200N23"), responseTwo, nil},
		{[]byte("11AB398765UJ1A050200N23"), responseThree, nil},
		{[]byte(""), nil, errors.New("Error, buffer could not be empty")},
		{[]byte("11AB3"), nil, errors.New("Error, buffer size is not correct, missing data")},
		{[]byte("04ABCDN3"), nil, errors.New("Error, missing data")},
		{[]byte("NNABCDN3"), nil, errors.New("Large cannot be parse string to int: NN")},
		{[]byte("02ABRNE"), nil, errors.New("Unknow type, must be Numeric or Alfanumeric, not: R")},
		{[]byte("02ABNER"), nil, errors.New("Value does not match Numeric type")},
		{[]byte("02??AER"), nil, errors.New("Value does not match Alfanumeric type")},
	}

	for _, tt := range tests {
		testname := fmt.Sprintf(string(tt.buffer), tt.wantResponse, tt.wantError)
		t.Run(testname, func(t *testing.T) {
			responseDecode, err := DecodeBuffer(tt.buffer)
			if !reflect.DeepEqual(responseDecode, tt.wantResponse) {
				t.Errorf("got %v, want %v", responseDecode, tt.wantResponse)
			}

			if err != nil {
				if err.Error() != tt.wantError.Error() {
					t.Errorf("got %v, want %v", err, tt.wantError)
				}
			}
		})
	}
}
